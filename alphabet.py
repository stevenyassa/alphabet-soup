def read_word_search(filename):
    with open(filename, 'r') as f:
        contents = f.read()

    lines = contents.splitlines()
    grid_size = tuple(map(int, lines[0].split('x')))
    num_rows, num_cols = grid_size
    grid = [line.split() for line in lines[1:num_rows+1]]
    words_to_find = lines[num_rows+1:]
    return grid, words_to_find


def solve_word_search(grid, words_to_find):
    num_rows, num_cols = len(grid), len(grid[0])
    results = []

    def find_word(word):
        for i in range(num_rows):
            for j in range(num_cols):
                if grid[i][j] == word[0]:
                    for di, dj in [(0, 1), (1, 0), (1, 1), (-1, 0), (0, -1), (-1, -1), (1, -1), (-1, 1)]:
                        if check_direction(word, i, j, di, dj):
                            start_idx = f"{i}:{j}"
                            end_idx = f"{i + (len(word) - 1) * di}:{j + (len(word) - 1) * dj}"
                            results.append(f"{word} {start_idx} {end_idx}")
                            return

    def check_direction(word, i, j, di, dj):
        for k in range(1, len(word)):
            if i + k * di < 0 or i + k * di >= num_rows or j + k * dj < 0 or j + k * dj >= num_cols:
                return False
            if grid[i + k * di][j + k * dj] != word[k]:
                return False
        return True

    for word in words_to_find:
        find_word(word)

    return results


if __name__ == '__main__':
    grid, words_to_find = read_word_search('word_search.txt')
    results = solve_word_search(grid, words_to_find)

    for result in results:
        print(result)
